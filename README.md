# Facebook Finder

Find posts in Facebook Groups according to whitelist and blacklist and get an email report of all the changes since the last run.

## Installation

```bash
git clone git@gitlab.com:hagar-yungman/FacebookFinder.git
```

## Configuration

Update `FacebookFinder/config.json` (or the `config.json` wherever the EXE is):

* `EmailAdresses` should contain list of emails the results will be sent to.
* `GmailLogin` should contain a gmail account that the mail will be sent from. The username shouldn't contain `@gmail.com` suffix.
* `FacebookLogin` should contain your facebook cardentials as entered in Facebook.
* `Groups` should contain all the groups you want to search in.
   * The `name` isn't required, it's only for the json to be human readable. 
   * The `id` is what appears in the URL after `/groups/`, for example: `https://www.facebook.com/groups/unimportant.facts/` the `id` is `unimportant.facts`.
   * Please make sure your Facebook account is registered to all the closed groups otherewith it wouldn't work.
* `WhiteList` should contain an array of arrays of regular expressions you want in the post. The outer array is combined using AND and the inner array is combined using OR.
* `BlackList` should contain an array of arrays of regular expressions you don't want in the post. The outer array is combined using AND and the inner array is combined using OR.

## Usage

Run it with Visual Studio or `mono`, or if you received an EXE from a friend make sure you run it and get hacked.

A file named `visited.json` will be created in the bin folder (or wherever the EXE is) to track posts from previous runs.
