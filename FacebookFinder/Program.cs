﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Coypu;
using Coypu.Drivers;
using Newtonsoft.Json.Linq;

namespace FacebookFinder
{
    static class Program
    {
        private static readonly string VisitedFilename = Path.Combine(Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory), @"visited.json");
        private static BrowserSession _session;

        private static void LoginToFacebook(string userName, string password)
        {
            _session.Visit("https://www.facebook.com/");
            _session.FillIn("Email or Phone").With(userName);
            _session.FillIn("Password").With(password);
            _session.ClickButton("Log In");
        }

        private static void NavigateToGroup(string groupId)
        {
            _session.Visit($"https://www.facebook.com/groups/{groupId}/");
            try
            {
                _session.FindCss("._n8._3qx.uiLayer._3qw", Options.NoWait).Click();
            }
            catch (FinderException)
            {
            }
            _session.FindCss("*[aria-label='Story']");
        }
        private static IEnumerable<SnapshotElementScope> GetGroupPosts()
        {
            _session.FindCss("._4-u2.mbm._5jmm._5pat._5v3q._4-u8");
            //if there is join group button
            if (_session.FindAllCss("._42ft _4jy0 _3o9h _3o9h _4jy4 _4jy2 selected _51sy").Any())
            {
                 throw new Exception("you need to join the group");
            }
            for (int i = 0; i < 2; i++)
            {
                string lastPost = _session.FindAllCss("._4-u2.mbm._5jmm._5pat._5v3q._4-u8").Last()["data-ft"];

                _session.ExecuteScript("window.scrollTo(0, document.body.scrollHeight)");
                try
                {
                    _session.RetryUntilTimeout(delegate
                    {
                        if (_session.FindAllCss("._4-u2.mbm._5jmm._5pat._5v3q._4-u8").Last()["data-ft"] == lastPost)
                        {
                            throw new FinderException("no new post");
                        }
                    });
                }
                catch (FinderException)
                {
                    break;
                }
            }
            return _session.FindAllCss("._4-u2.mbm._5jmm._5pat._5v3q._4-u8");
        }

        private static string GetPostText(SnapshotElementScope post)
        {
            try
            {
                post.ClickLink("See More", Options.Merge(Options.First, Options.NoWait));
            }
            catch (FinderException)
            {
            }
            catch (InvalidOperationException)
            {
                _session.ExecuteScript("window.scrollTo(0, window.scrollY - 100)");
                post.ClickLink("See More", Options.First);
            }
            string input = post.Text;
            string pattern = "\r\nLike\r\n.*";
            input = Regex.Replace(input, pattern, "", RegexOptions.Singleline);
            Regex rgx = new Regex("\n.*\n");
            input = rgx.Replace(input,"", 1);
            return input;
        }

        private static bool IsWordInListGroup(string input, JArray groups)
        {
            foreach (JArray group in groups)
            {
                bool found = false;
                foreach (string word in group)
                {
                    if (Regex.IsMatch(input, word))
                    {
                        found = true;
                        break;
                    }
                }
                if (!found) return false;
            }
            return true;
        }

        private static void SendMail(string mailBody, JArray emailAdresses, JObject gmailSenderLogin)
        {
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress("facebfinder@gmail.com");
            foreach (string email in emailAdresses)
            {
                mail.To.Add(email);
            }
            SmtpClient client = new SmtpClient();
            client.Port = 587;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential((string)gmailSenderLogin["userName"], (string)gmailSenderLogin["password"]);
            client.EnableSsl = true;
            client.Host = "smtp.gmail.com";
            mail.Subject = "I found something!!!!!";
            mail.Body = mailBody;
            client.Send(mail);
        }
        private static void FindPostInGroup(JToken groupjson, JArray whiteList, JArray blackList, HashSet<string> visited, Dictionary<string, LinkedList<string>>  mailBody)
        {
            string groupId = (string)groupjson["id"];
            NavigateToGroup(groupId);
            //int postsFound = 0;
            SnapshotElementScope[] posts = GetGroupPosts().ToArray();
            int postnum = 0;
            foreach (SnapshotElementScope post in posts)
            {
                postnum++;
                Console.WriteLine($"{postnum} / {posts.Length}");
                var input = GetPostText(post);
                string advertisor = Regex.Replace(input, "\r.*", "", RegexOptions.Singleline);
                input = new Regex("^.*\r").Replace(input,"" , 1);
                if (!IsWordInListGroup(input, whiteList))
                {
                    continue;
                }
                if (IsWordInListGroup(input, blackList))
                {
                    continue;
                }
                if (!visited.Contains(input))
                {
                    //postsFound++;
                    visited.Add(input);
                    string postUrl =
                        post.FindAllXPath(".//span[@class = 'timestampContent'][1]/ancestor::a[1]").First()["href"];
                    if (!mailBody.ContainsKey(advertisor))
                    {
                        mailBody[advertisor] = new LinkedList<string>();
                    }
                    mailBody[advertisor].AddLast(postUrl);
                }

            }
        }

        public static void Main(string[] args)
        {
            SessionConfiguration sessionConfiguration = new SessionConfiguration();
            sessionConfiguration.Timeout = TimeSpan.FromSeconds(5);
            //sessionConfiguration.Driver = typeof (ChromeDriver);
            sessionConfiguration.Browser = Browser.Chrome;
            _session = new BrowserSession(sessionConfiguration);

            Dictionary<string, LinkedList<string>> mailBody = new Dictionary<string, LinkedList<string>>();
            HashSet<string> visited = File.Exists(VisitedFilename) ? new HashSet<string>(JArray.Parse(File.ReadAllText(VisitedFilename)).ToObject<string[]>()) : new HashSet<string>();
            
            JObject jsonConfig =
                (JObject) JObject.Parse(
                    File.ReadAllText(@"config.json"));
            JArray groupsArray = (JArray) jsonConfig["Groups"];
            JArray blackList = (JArray) jsonConfig["BlackList"];
            JArray whiteList = (JArray) jsonConfig["WhiteList"];
            JArray emailAdresses = (JArray) jsonConfig["EmailAdresses"];
            JObject facebookLogin = (JObject) jsonConfig["FacebookLogin"];
            JObject gmailSenderLogin = (JObject) jsonConfig["GmailLogin"];

            LoginToFacebook((string) facebookLogin["userName"], (string) facebookLogin["password"]);

            int groupIndex = 0;
            foreach (var groupArray in groupsArray)
            {
                groupIndex++;
                Console.WriteLine($"group: {groupIndex} / {groupsArray.Count}");

                FindPostInGroup(groupArray, whiteList, blackList, visited, mailBody);
            }
            if (mailBody.Count != 0)
            {
                LinkedList<string> body = new LinkedList<string>();
                foreach (KeyValuePair<string, LinkedList<string>> advertisorLinks in mailBody)
                {
                    body.AddLast($"\n\n{advertisorLinks.Key}:\n");
                    body.AddLast(string.Join("\n", advertisorLinks.Value));
                }
                SendMail(string.Join("", body), emailAdresses, gmailSenderLogin);
            }
            File.WriteAllText(VisitedFilename, new JArray(visited.ToArray()).ToString());

            _session.Dispose();
        }

        private static void FixVisitedFile()
        {
            JArray visited = JArray.Parse(File.ReadAllText(VisitedFilename));
            HashSet<string> fixedfile = new HashSet<string>();
            foreach (var post in visited)
            {
                string postString = (string) post.ToString();
                string advertisor = Regex.Replace(postString, "\r.*", "", RegexOptions.Singleline);
                fixedfile.Add(advertisor);
            }
            File.WriteAllText(VisitedFilename, new JArray(fixedfile.ToArray()).ToString());
        }
    }
}
